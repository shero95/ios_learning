//
//  ViewController.swift
//  Guess
//
//  Created by dmy on 2019/9/11.
//  Copyright © 2019 dmy. All rights reserved.
//



import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var doraGuess: UIImageView!
    @IBOutlet weak var yourGuess: UIImageView!
    @IBAction func guess(_ sender: Any) {
        doraGuess.image = UIImage(named:"rock" )
        
        let guess = arc4random_uniform(3)
        var result = ""
        if 0 == guess {
            yourGuess.image = UIImage(named: "scissor")
            result = "you loss"
        }else if 1 == guess{
            yourGuess.image = UIImage(named: "rock")
            result = "打平"
        }else{
            yourGuess.image = UIImage(named: "paper")
            result = "you win"
        }
        let alert = UIAlertController(title: "比赛结束",message:result,preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "确定", style: .default
            , handler: {_ in
                self.doraGuess.image = nil
                self.yourGuess.image = nil
        }))
        present(alert,animated: true,completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
}
