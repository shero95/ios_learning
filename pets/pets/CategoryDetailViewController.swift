//
//  CategoryDetailViewController.swift
//  Memorandum
//
//  Created by CurryZhang on 2019/9/25.
//  Copyright © 2019 zhaofang. All rights reserved.
//

import UIKit
protocol CategoryDetailViewControllerDelegate: class {
    func categoryDetailViewController(_ controller: CategoryDetailViewController, didFinishAdding category: TodoCategory)
    func categoryDetailViewController(_ controller: CategoryDetailViewController, didFinishEditing category: TodoCategory)
}

class CategoryDetailViewController: UITableViewController, UITextFieldDelegate, IconPickerViewControllerDelegate{
    func iconPicker(_ picker: IconPickerViewController, didPick iconName: String) {
        self.imageName = iconName
        iconImage.image = UIImage(named: imageName)
        
        navigationController?.popViewController(animated: true)
    }
    
    weak var delegate: CategoryDetailViewControllerDelegate?
    
    var categoryToEdit: TodoCategory?
    var imageName: String = "Folder"

    
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var iconImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let category = categoryToEdit {
            title = "重命名"
            inputTextField.text = category.name
            self.imageName = category.iconImage
        }else {
            title = "新增分类"
        }
        iconImage.image = UIImage(named: imageName)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        inputTextField.becomeFirstResponder()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string)
        doneBarButton.isEnabled = newText.count > 0
        
        return true
        
    }
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        }else {
            return nil
        }
    }

    @IBAction func done(_ sender: Any) {
        if let category = categoryToEdit {
            category.name = inputTextField.text!
            category.iconImage = imageName
            delegate?.categoryDetailViewController(self, didFinishEditing: category)
        }else {
            let category = TodoCategory(name: inputTextField.text!)
            category.iconImage = imageName
            delegate?.categoryDetailViewController(self, didFinishAdding: category)
        }
       
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PickIcon" {
            let controller = segue.destination as! IconPickerViewController
            controller.delegate = self
        }
    }
}
