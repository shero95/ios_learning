//
//  TodoViewController.swift
//  Memorandum
//
//  Created by zhaofang on 2019/9/23.
//  Copyright © 2019 CurryZhang. All rights reserved.
//

import UIKit

class TodoViewController: UITableViewController, AddItemViewControllerDelegate {
    var category: TodoCategory!
    
    required init?(coder aDecoder: NSCoder) {
       
        
        super.init(coder: aDecoder)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let category = category {
                title = category.name
        }
    
        
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TodolistItem", for: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodolistItem", for: indexPath)
        let textLabel = cell.viewWithTag(1000) as! UILabel
        
        let item = category.items[indexPath.row]
        textLabel.text = item.text
        configCellCheckmark(for: cell, at: indexPath)
        return cell
    }
    func configCellCheckmark(for cell: UITableViewCell, at indexPath: IndexPath){
        let checkLabel = cell.viewWithTag(1001) as! UILabel
        let item = category.items[indexPath.row]
        if item.checked {
            checkLabel.text = "√"
        } else {
            checkLabel.text = ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath){
            let item = category.items[indexPath.row]
            item.checked = !item.checked
            configCellCheckmark(for: cell, at: indexPath)
            
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        category.items.remove(at: indexPath.row)
        
        let paths = [indexPath]
        
        tableView.deleteRows(at: paths, with: .automatic)
        
    }
    
    func AddItemViewController(_ controller: AddItemViewController, endAdding item: TodoItem) {
        let newRow = category.items.count
        category.items.append(item)

        let indexPath = IndexPath(row: newRow, section: 0)
        let paths = [indexPath]
        tableView.insertRows(at: paths, with: .automatic)
    }
    
    func AddItemViewController(_ controller: AddItemViewController, endEditing item: TodoItem) {
        if let index = category.items.firstIndex(where: {$0 === item}) {
            category.items[index] = item
            
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                let textLabel = cell.viewWithTag(1000) as! UILabel
                textLabel.text = item.text
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItem" {
            let dest = segue.destination as! UINavigationController
            let addItemViewController = dest.topViewController as! AddItemViewController
            addItemViewController.delegate = self
        }else if segue.identifier == "EditItem"{
            let dest = segue.destination as! UINavigationController
            let addItemViewController = dest.topViewController as! AddItemViewController
            addItemViewController.delegate = self
            if let index = tableView.indexPath(for: sender as! UITableViewCell) {
                addItemViewController.itemToEdit = category.items[index.row]
            }
        }
    }
}
