//
//  IconPickerViewController.swift
//  Memorandum
//
//  Created by CurryZhang on 2019/10/9.
//  Copyright © 2019 zhaofang. All rights reserved.
//

import UIKit
protocol IconPickerViewControllerDelegate: class {
    func iconPicker(_ picker: IconPickerViewController,didPick iconName: String)
}

class IconPickerViewController: UITableViewController {
    weak var delegate: IconPickerViewControllerDelegate?
    
    let icons = ["NO Icon",
                "Appointments",
                "Birthdays",
                "Chores",
                "Drinks",
                "Folder",
                "Groceries",
                "Inbox",
                "Photos",
                "Trips"
    ]
    let iconNames = ["不显示图标",
                    "金毛狗",
                    "雪兔",
                    "鹦鹉",
                    "小龟",
                    "仓鼠",
                    "金鱼",
                    "黑土",
                    "兔",
                    "海洋鱼"
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

  
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return icons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IconCell", for: indexPath)
        
        cell.textLabel!.text = iconNames[indexPath.row]
        cell.imageView!.image = UIImage(named: icons[indexPath.row])
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            let iconName = icons[indexPath.row]
            delegate.iconPicker(self, didPick: iconName)
        }
    }

}
