//
//  TodoCategory.swift
//  Memorandum
//
//  Created by CurryZhang on 2019/9/25.
//  Copyright © 2019 zhaofang. All rights reserved.
//

import UIKit

class TodoCategory: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "Name")
        aCoder.encode(items, forKey: "Items")
        aCoder.encode(iconImage, forKey: "IconImage")
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "Name") as! String
        items = aDecoder.decodeObject(forKey: "Items") as! [TodoItem]
        iconImage = aDecoder.decodeObject(forKey: "IconImage") as! String
        super.init()
    }
    

    var name = ""
    var iconImage = "No Icon"
    var items = [TodoItem]()
    
    init(name: String) {
        self.name = name
        super.init()
    }
    
    func countItems() -> Int {
        var num = 0
        for item in items where !item.checked{
            num += 1
        }
        return num
    }
}
